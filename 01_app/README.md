# Juls_toolbox

## Table of Contents
1. [Description](#description)
2. [Installation](#installation)
3. [License](#license)
4. [Collaboration](#collaboration)
5. [Author](#author)

## Description
***
Juls_toolbox is a Python package to generate a Maya shelf providing a list of useful tools. 
The shelf will be recreated from scratch at every start of Maya to insure a clear and updated version of every tool.

## Installation
***
This is a script package, so you can launch all tools individually as the 
standard Maya scripts. However, some scripts needs some internal libraries 
to be imported to work.

Here is the standard way to install the package to insure it to work properly.

1. Clone the git repository ```git clone https://github.com/SegretoVfx/advance_python_workshop.git```
2. Copy the ```userSetup.py``` file into the maya script folder located in :```C:\Users\<user name>\Documents\maya\scripts```
3. In the copied version of the ```userSetup.py``` file, localize the 
   ```PATH``` variable.
4. Change the value to point toward the cloned folder:
```PATH = "D:\path\to\folder\scripts"```.
5. Restart Maya

The toolbox will appear in the Maya shelf.

## License
***
[MIT](https://choosealicense.com/licenses/mit/)

Copyright (c) 2024 Julien Segreto 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Collaboration
***
I don't know yet.

## Author
***
Julien Segreto
segretovfx@gmail.com


